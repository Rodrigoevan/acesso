package br.com.mastertech.consumer;

import br.com.mastertech.producer.Acesso;
import org.springframework.kafka.annotation.KafkaListener;
import org.springframework.messaging.handler.annotation.Payload;
import org.springframework.stereotype.Component;

@Component
public class AcessoConsumer {

    @KafkaListener(topics = "spec2-rodrigo-carvalho-1", groupId = "rocarev-1")
    public void receber(@Payload Acesso acesso){
        System.out.println("Recebi acesso do cliente " + acesso.getCliente() + " da porta: " + acesso.getPorta());
    }
}
