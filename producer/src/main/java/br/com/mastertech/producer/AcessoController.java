package br.com.mastertech.producer;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/acesso")
public class AcessoController {

    @Autowired
    AcessoProducer acessoProducer;
    @Autowired
    AcessoService acessoService;

    @GetMapping("/{cliente}/{porta}")
    public void create(@PathVariable Long cliente, @PathVariable Long porta){
        boolean acessoValido = acessoService.validaAcesso();
        Acesso acesso = new Acesso();
        acesso.setCliente(cliente);
        acesso.setPorta(porta);
        System.out.println("Producer Cliente :" + acesso.getCliente() + "porta :" + acesso.getPorta());
        if (acessoValido == true){
            acessoProducer.enviarAoKafka(acesso);
        }

    }
}
