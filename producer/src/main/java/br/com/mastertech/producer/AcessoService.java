package br.com.mastertech.producer;

import org.springframework.stereotype.Service;

import java.util.Random;

@Service
public class AcessoService {

    public boolean validaAcesso(){
        System.out.println("random : " + getRandomBoolean());
        return getRandomBoolean();
    }

    public boolean getRandomBoolean() {
        Random random = new Random();
        return random.nextBoolean();
    }
}
